{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.bluetooth;
in {
  options.modules.hardware.bluetooth = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    hardware.bluetooth.enable = true;

    services.pipewire.wireplumber.configPackages = [
      (pkgs.writeTextDir
        "share/wireplumber/bluetooth.lua.d/51-bluez-config.lua" ''
          bluez_monitor.properties = {
            ["bluez5.enable-sbc-xq"] = true,
            ["bluez5.enable-msbc"] = true,
            ["bluez5.enable-hw-volume"] = true,
            ["bluez5.headset-roles"] = "[ hsp_hs hfp_hf a2dp_sink ]"
          }
        '')
    ];
  };
}
