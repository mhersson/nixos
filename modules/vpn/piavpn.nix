{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.vpn.piavpn;
in {
  options.modules.vpn.piavpn = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    sops.secrets.privateKey = { sopsFile = ../../secrets/piavpn.yaml; };

    networking.wg-quick.interfaces = {
      wg0 = {
        address = [ "10.7.137.78/32" ];
        dns = [ "10.0.0.243" ];
        privateKeyFile = config.sops.secrets.privateKey.path;

        peers = [{
          publicKey = "i2QznQI+TqTTriJD0r6A/zZqcd2aAhwGMqsecpH6Mig=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "46.246.122.169:1337";
          persistentKeepalive = 25;
        }];
      };
    };

  };
}
