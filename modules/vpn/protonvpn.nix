{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.vpn.protonvpn;
in {
  options.modules.vpn.protonvpn = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    sops.secrets.privateKey = { sopsFile = ../../secrets/protonvpn.yaml; };

    networking.wg-quick.interfaces = {
      wg0 = {
        address = [ "10.2.0.2/32" ];
        dns = [ "10.2.0.1" ];
        privateKeyFile = config.sops.secrets.privateKey.path;

        peers = [{
          publicKey = "sSbgwNAoZtBVWlg6ZLnFDrXTM3YFTpPVKgE4DtzSUw0=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "146.70.170.2:51820";
          persistentKeepalive = 25;
        }];
      };
    };

  };
}
