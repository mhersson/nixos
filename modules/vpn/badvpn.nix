{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.vpn.badvpn;
in {
  options.modules.vpn.badvpn = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ badvpn openvpn ];

    security.sudo.extraRules = [
      {
        groups = [ "wheel" ];
        commands = [{
          command = "/run/current-system/sw/bin/ip";
          options = [ "NOPASSWD" ];
        }];
      }
      {
        groups = [ "wheel" ];
        commands = [{
          command = "/run/current-system/sw/bin/openvpn";
          options = [ "NOPASSWD" ];
        }];
      }
    ];

    # TODO: add shellaliases
    # environment.shellAliases = {};
  };
}
