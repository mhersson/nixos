# tlp. nix
{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.tlp;
in {
  options.modules.services.tlp = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    services.tlp = {
      enable = true;

      settings = {
        CPU_SCALING_GOVERNOR_ON_AC = "performance";
        CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
        START_CHARGE_THRESH_BAT0 = 80;
        STOP_CHARGE_THRESH_BAT0 = 90;
      };
    };
  };
}
