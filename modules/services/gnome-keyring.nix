# tlp. nix
{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.gnome-keyring;
in {
  options.modules.services.gnome-keyring = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    services.gnome.gnome-keyring = {
      enable = true;
    };
  };
}
