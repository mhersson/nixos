# podman. nix -- not really a service this,
# but couldn't agree with myself on where to put it
{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.podman;
in {
  options.modules.services.podman = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    virtualisation.podman.enable = true;

    # Until I figure out how to get it working rootless this is needed for minikube
    security.sudo.extraRules = [{
      groups = [ "wheel" ];
      commands = [{
        command = "${pkgs.podman}/bin/podman";
        options = [ "NOPASSWD" ];
      }];
    }];

  };
}
