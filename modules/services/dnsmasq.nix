{ options, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.dnsmasq;
in {
  options.modules.services.dnsmasq = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    networking.networkmanager.dns = "dnsmasq";

    environment.etc = {
      "NetworkManager/dnsmasq.d/skynet.conf".text = ''
        server=/bamsebo.lan/192.168.1.138
        address=/apps.skynet.bamsebo.eu/192.168.1.200
        address=/api.skynet.bamsebo.eu/192.168.1.200
      '';
    };
  };
}
