{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.editors.emacs;
  configDir = config.dotfiles.configDir;
in {
  options.modules.editors.emacs = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      ## Emacs itself
      binutils # native-comp needs 'as', provided by this

      ((emacsPackagesFor emacs29-pgtk).emacsWithPackages
        (epkgs: [ epkgs.vterm ]))

      ## Doom dependencies
      git
      (ripgrep.override { withPCRE2 = true; })
      gnutls # for TLS connectivity

      ## Optional dependencies
      fd # faster projectile indexing
      imagemagick # for image-dired
      (mkIf (config.modules.lang.java.enable)
        graphviz-nox) # for plantuml preview
      (mkIf (config.programs.gnupg.agent.enable)
        pinentry_emacs) # in-emacs gnupg prompts
      zstd # for undo-fu-session/undo-tree compression
      multimarkdown # for markdown preview

      ## Module dependencies
      # :checkers spell
      (aspellWithDicts (ds: with ds; [ nb en en-computers en-science ]))
      # :lang nix
      nixfmt
      # :lang rust
      rustfmt
      rust-analyzer
    ];

    env.PATH = [ "$XDG_CONFIG_HOME/emacs/bin" ];

    environment.shellAliases = { emacs = "emacs -nw"; };

    fonts.fonts = [ pkgs.emacs-all-the-icons-fonts ];
  };
}
