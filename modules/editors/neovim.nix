{ config, options, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.neovim;
in {
  options.modules.editors.neovim = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.neovim-nightly-overlay.overlay ];

    environment.systemPackages = with pkgs; [
      neovim
      tree-sitter
      gcc
      (ripgrep.override { withPCRE2 = true; })
      lua
      nixfmt-classic
      nil # nix language server
      lua-language-server
      marksman
      stylua
      nodejs
      nodePackages.npm
    ];

    environment.shellAliases = {
      vi = "nvim";
      vim = "nvim";
    };
  };
}
