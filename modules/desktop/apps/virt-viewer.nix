{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.virt-viewer;
in {
  options.modules.desktop.apps.virt-viewer = { enable = mkEnableOption false; };

  config = mkIf cfg.enable { environment.systemPackages = with pkgs; [ virt-viewer ]; };
}
