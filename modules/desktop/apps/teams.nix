{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.teams;
in {
  options.modules.desktop.apps.teams = { enable = mkBoolOpt false; };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ teams ]; };
}
