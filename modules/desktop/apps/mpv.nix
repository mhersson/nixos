{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.mpv;
in {
  options.modules.desktop.apps.mpv = { enable = mkEnableOption false; };

  config = mkIf cfg.enable { environment.systemPackages = with pkgs; [ mpv ]; };
}
