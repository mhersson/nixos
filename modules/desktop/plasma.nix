{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.plasma;

in {
  options.modules.desktop.plasma = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    services.xserver = {
      enable = true;
      excludePackages = [ pkgs.xterm ];
      desktopManager.plasma5.enable = true;
      displayManager = {
        sddm.enable = true;
        defaultSession = "plasmawayland";
      };
    };

    environment.systemPackages = with pkgs; [ yakuake wl-clipboard ];

    environment.plasma5.excludePackages = with pkgs.libsForQt5; [
      ark
      elisa
      kmail
      korganizer
      konsole
      oxygen
      khelpcenter
      plasma-browser-integration
      print-manager
    ];

    # Ensure GTK apps use the same theme as KDE apps
    programs.dconf.enable = true;

    environment.sessionVariables = {
      MOZ_ENABLE_WAYLAND = "1";
      NIXOS_OZONE_WL = "1";
    };
  };
}
