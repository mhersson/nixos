{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.services.flatpak;
in {
  options.modules.desktop.services.flatpak = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    services.flatpak.enable = true;

    xdg.portal.enable = true;
    xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  };
}
