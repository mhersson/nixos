{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.services.redshift;
in {
  options.modules.desktop.services.redshift = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable { services.redshift.enable = true; };
}
