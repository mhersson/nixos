{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.services.picom;
in {
  options.modules.desktop.services.picom = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    services.picom = {
      enable = true;
      backend = "glx";
      shadow = false;
      fade = true;
      vSync = true;
    };

  };
}
