{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.i3;

in {
  options.modules.desktop.i3 = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      maim
      rofi
      xclip
      hsetroot
      dunst
      playerctl
      networkmanagerapplet
    ];

    services = {
      blueman.enable = true;
      xserver = {
        enable = true;
        excludePackages = [ pkgs.xterm ];
        layout = "us,no";
        xkbOptions = "terminate:ctrl_alt_bksp,grp:caps_toggle";
        displayManager = {
          lightdm.enable = true;
          lightdm.greeters.slick.enable = true;
          defaultSession = "none+i3";
          autoLogin = {
            enable = false;
            user = config.user.name;
          };
        };
        windowManager.i3 = {
          enable = true;
          package = pkgs.i3-gaps;
        };
      };
    };

  };
}
