{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.hyprland;
in {
  options.modules.desktop.hyprland = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    programs = {
      hyprland = {
        enable = true;
        xwayland = { enable = false; };
      };
      waybar.enable = true;
      thunar.enable = true;
    };

    services.blueman.enable = true;

    environment.systemPackages = with pkgs; [
      dunst
      fastfetch
      fuzzel
      gammastep
      grim
      hyprpaper
      hypridle
      hyprlock
      playerctl
      slurp
      swappy
      vimix-cursor-theme
      wl-clipboard
    ];

    xdg = {
      portal = {
        enable = true;
        extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
      };
    };

    environment.sessionVariables = { NIXOS_OZONE_WL = "1"; };

    # This is so waybar can display if the VPN is up
    security.sudo.extraRules = [{
      groups = [ "wheel" ];
      commands = [{
        command = "/run/current-system/sw/bin/wg";
        options = [ "NOPASSWD" ];
      }];
    }];
  };
}
