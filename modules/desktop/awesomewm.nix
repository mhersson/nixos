{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.awesomewm;

in {
  options.modules.desktop.awesomewm = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    nixpkgs.overlays = [
      (self: super: {
        awesome = super.awesome.override { lua = super.luajit; };
      })
    ];

    environment.systemPackages = with pkgs; [
      luajit
      rofi
      maim
      xclip
      networkmanagerapplet
      whitesur-icon-theme
      playerctl
    ];

    programs.dconf.enable = true;

    services = {
      blueman.enable = true;
      xserver = {
        enable = true;
        excludePackages = [ pkgs.xterm ];
        layout = "us,no";
        xkbOptions = "terminate:ctrl_alt_bksp,grp:caps_toggle";
        displayManager = {
          lightdm.enable = true;
          lightdm.greeters.slick.enable = true;
          defaultSession = "none+awesome";
          autoLogin = {
            enable = false;
            user = config.user.name;
          };
        };
        windowManager.awesome.enable = true;
        xautolock = {
          enable = true;
          time = 60;
          locker = "/run/current-system/sw/bin/systemctl suspend";
        };
      };
    };

  };
}
