{ config, options, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.spotifyd;
in {
  options.modules.shell.spotifyd = with types; { enable = mkBoolOpt false; };

  config =
    mkIf cfg.enable { environment.systemPackages = with pkgs; [ spotifyd spotify-tui ]; };
}
