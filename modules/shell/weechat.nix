{ config, options, pkgs, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.weechat;
in {
  options.modules.shell.weechat = with types; { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      weechat
      libnotify
      (aspellWithDicts (ds: with ds; [ nb en en-computers en-science ]))
    ];

  };
}
