{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.shell.gnupg;
  plasmaEnabled = config.modules.desktop.plasma.enable;
in {
  options.modules.shell.gnupg = with types; {
    enable = mkBoolOpt false;
    cacheTTL = mkOpt int 14400; # 4hr
  };

  config = mkIf cfg.enable {
    programs.gnupg.agent = {
      enable = true;
      # If plasma is not enabled, use pinentry-gnome3
      pinentryPackage = mkIf (!plasmaEnabled) pkgs.pinentry-gnome3;
    };

    home.file.".gnupg/gpg-agent.conf" = {
      text = ''
        default-cache-ttl ${toString cfg.cacheTTL}
      '';
    };
  };
}
