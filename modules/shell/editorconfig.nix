{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.editorconfig;
in {
  options.modules.shell.editorconfig = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.editorconfig-core-c ];
  };
}
