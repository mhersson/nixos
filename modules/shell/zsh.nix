{ config, options, pkgs, lib, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.shell.zsh;

in {
  options.modules.shell.zsh = with types; {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    users.defaultUserShell = pkgs.zsh;

    programs.zsh = {
      enable = true;
      enableCompletion = true;

      enableGlobalCompInit = false;
    };

    environment.systemPackages = with pkgs; [ zsh nix-zsh-completions bat fzf jq ];

  };
}
