# modules/lang/rust.nix --- https://rust-lang.org

{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.lang.rust;
in {
  options.modules.lang.rust = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      rustc
      cargo
      rustfmt
      clippy
      rust-analyzer
    ];
  };
}
