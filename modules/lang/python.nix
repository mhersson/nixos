# modules/lang/python.nix --- https://godotengine.org/

{ config, options, lib, pkgs, my, ... }:

with lib;
with lib.my;
let cfg = config.modules.lang.python;
in {
  options.modules.lang.python = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ python3 ];
  };
}
