{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.lang.java;
in {
  options.modules.lang.java = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    programs.java.enable = true;
  };
}
