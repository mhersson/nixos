# modules/lang/go.nix --- https://go.dev

{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.lang.go;
in {
  options.modules.lang.go = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ go ];

    env.GOPATH = "$HOME/Development/go";
    env.PATH = [ "$GOPATH/bin" ];
  };
}
