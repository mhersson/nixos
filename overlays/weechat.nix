self: super: {
  weechat = super.weechat.override {
    configure = { availablePlugins, ... }: {
      plugins = with availablePlugins; [
        (python.withPackages (ps: with ps; [ websocket-client ]))
        (perl.withPackages (ps: with ps; [ PodParser ]))
      ];
    };
  };
}
