[![Made with Doom Emacs](https://img.shields.io/badge/Made_with-Doom_Emacs-blueviolet.svg?style=flat-square&logo=GNU%20Emacs&logoColor=white)](https://github.com/hlissner/doom-emacs)
[![NixOS Unstable](https://img.shields.io/badge/NixOS-unstable-blue.svg?style=flat-square&logo=NixOS&logoColor=white)](https://nixos.org)

## Quick start

1. Acquire NixOS 21.11 or newer:

   ```sh
   # Download nixos-unstable
   wget -O nixos.iso https://channels.nixos.org/nixos-unstable/latest-nixos-minimal-x86_64-linux.iso

   # Write it to a flash drive
   dd if=nixos.iso of=/dev/sdX bs=4M
   ```

2. Boot into the installer.

3. Switch to root user: `sudo -i`

4. Do your partitions and mount your root to `/mnt`

5. Install these dotfiles:

   ```sh
   nix-shell -p git

   # Set HOST to the desired hostname of this system
   HOST=...
   # Set USER to your desired username (defaults to morten)
   USER=...

   git clone https://gitlab.com/mhersson/nixos /mnt/etc/nixos
   cd /mnt/etc/nixos

   # Create a host config in `hosts/` and add it to the repo:
   mkdir -p hosts/$HOST
   nixos-generate-config --root /mnt --dir /etc/nixos/hosts/$HOST
   rm -f hosts/$HOST/configuration.nix
   cp hosts/megatron/default.nix hosts/$HOST/default.nix
   vim hosts/$HOST/default.nix  # configure this for your system; don't use it verbatim!
   git add hosts/$HOST

   # Install nixOS
   USER=$USER nixos-install --root /mnt --flake .#$HOST
   ```

6. Then reboot and you're good to go!

> :warning: **Don't forget to change your `root` and `$USER` passwords!** They
> are set to `nixos` by default.
