{ pkgs, config, lib, ... }: {
  imports = [ ../home.nix ./hardware-configuration.nix ];

  ## Modules
  modules = {
    desktop = {
      hyprland.enable = true;

      apps = {
        brave.enable = false;
        firefox.enable = true;
        mpv.enable = true;
        signal.enable = true;
        slack.enable = true;
        spotify.enable = false;
        steam.enable = false;
        virt-viewer.enable = true;
      };

      services = {
        flatpak.enable = false;
        picom.enable = false;
        redshift.enable = false;
      };

      term = {
        alacritty.enable = true;
        kitty.enable = false;
      };
    };

    editors = {
      default = "nvim";
      emacs.enable = false;
      neovim.enable = true;
    };

    lang = {
      go.enable = true;
      java.enable = false;
      python.enable = true;
      rust.enable = true;
      shell.enable = true;
    };

    services = {
      dnsmasq.enable = false;
      gnome-keyring.enable = false;
      podman.enable = true;
      ssh.enable = true;
    };

    shell = {
      direnv.enable = true;
      editorconfig.enable = true;
      gnupg.enable = true;
      lazygit.enable = false;
      pass.enable = true;
      ranger.enable = false;
      spotifyd.enable = false;
      tmux.enable = true;
      weechat.enable = true;
      zsh.enable = true;
    };

    vpn = {
      badvpn.enable = true;
      protonvpn.enable = false;
      piavpn.enable = true;
    };

    hardware = {
      audio.enable = true;
      bluetooth.enable = true;
    };
  };

  hardware.opengl.enable = true;

  ## Local config
  programs.ssh.startAgent = true;
  services.openssh.startWhenNeeded = true;

  services.fstrim.enable = true;

  networking.networkmanager.enable = true;
  # The global useDHCP flag is deprecated, therefore explicitly set to false
  # here. Per-interface useDHCP will be mandatory in the future, so this
  # generated config replicates the default behaviour.
  networking.useDHCP = false;
}
