{ config, lib, pkgs, ... }:

with lib; {
  networking.extraHosts = ''
    192.168.1.196   gitlab.bamsebo.lan
  '';

  networking.firewall.enable = true;
  networking.firewall.allowedTCPPorts = [ 22 ];

  networking.nameservers =
    [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];

  services.resolved = {
    enable = true;
    domains = [ "~." ];
    fallbackDns = [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];
  };

  ## Location config
  time.timeZone = mkDefault "Europe/Oslo";
  i18n.defaultLocale = mkDefault "en_US.UTF-8";

  # For redshift, mainly
  location = (if config.time.timeZone == "Europe/Oslo" then {
    latitude = 59.66333;
    longitude = 10.62975;
  } else
    { });

  fonts = {
    packages = with pkgs; [
      (nerdfonts.override { fonts = [ "JetBrainsMono" "Hack" "UbuntuMono" ]; })
      noto-fonts
      noto-fonts-emoji
    ];

    fontconfig.defaultFonts = {
      monospace = [ "JetBrainsMono Nerd Font" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };

}
