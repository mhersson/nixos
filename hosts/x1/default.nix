{ pkgs, config, lib, ... }: {
  imports = [ ../home.nix ./hardware-configuration.nix ];

  ## Modules
  modules = {
    desktop = {
      i3.enable = true;

      apps = {
        brave.enable = true;
        firefox.enable = true;
        mpv.enable = false;
        signal.enable = true;
        slack.enable = true;
        spotify.enable = false;
        steam.enable = false;
        teams.enable = false;
        virt-viewer.enable = false;
      };

      services = {
        flatpak.enable = false;
        picom.enable = true;
        redshift.enable = true;
      };

      term = {
        alacritty.enable = false;
        kitty.enable = true;
      };
    };

    editors = {
      default = "nvim";
      emacs.enable = true;
      neovim.enable = true;
    };

    lang = {
      java.enable = true;
      go.enable = true;
      python.enable = true;
      rust.enable = true;
      shell.enable = true;
    };

    services = {
      dnsmasq.enable = false;
      gnome-keyring.enable = true;
      podman.enable = true;
      ssh.enable = true;
      tlp.enable = true;
    };

    shell = {
      direnv.enable = true;
      editorconfig.enable = true;
      gnupg.enable = true;
      lazygit.enable = false;
      pass.enable = true;
      ranger.enable = false;
      spotifyd.enable = false;
      tmux.enable = false;
      weechat.enable = false;
      zsh.enable = true;
    };

    vpn = {
      badvpn.enable = true;
      protonvpn.enable = true;
    };

    hardware = {
      audio.enable = true;
      bluetooth.enable = true;
    };

  };

  services.xserver = {
    resolutions = [
      {
        x = 1920;
        y = 1080;
      }
      {
        x = 2560;
        y = 1440;
      }
      {
        x = 5120;
        y = 1440;
      }
    ];

    libinput = {
      enable = true;
      touchpad.tapping = false;
      touchpad.naturalScrolling = false;
    };

  };

  # Enable backlight controls
  programs.light.enable = true;

  hardware.opengl.enable = true;

  ## Local config
  programs.ssh.startAgent = true;
  services.openssh.startWhenNeeded = true;

  services.fstrim.enable = true;

  networking.networkmanager.enable = true;
  # The global useDHCP flag is deprecated, therefore explicitly set to false
  # here. Per-interface useDHCP will be mandatory in the future, so this
  # generated config replicates the default behaviour.
  networking.useDHCP = false;
}
